REVEAL_META = {
    # Title of the slide
    "title": "Dual Comb Swept Wavelength Interferometry for SDM Device Characterisation",
    # Author in the metadata of the slide
    "author": "Jochen Schröder",
    # Description in the metadata of the slide
    "description": "Summer Topicals Talk 2022"
}


REVEAL_PLUGINS = ["RevealMenu", 
                    "RevealMarkdown",
                   "RevealHighlight",
                   "RevealSearch",
                   "RevealNotes",
                   "RevealMath",
                   "RevealZoom"]
REVEAL_EXTRA_SCRIPTS = [ "plugins/menu/menu.js"]
